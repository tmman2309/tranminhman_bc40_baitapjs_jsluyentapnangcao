// Bài 1: In một bảng số từ 1-100
let inBangSo = () => {
  let content1 = "";
  let n = 0;

  // Vòng lặp 1
  for (let index = 1; index <= 10; index++) {
    let content2 = "";

    // Vòng lặp 2
    for (let i = 1; i <= 10; i++) {
      let soDuocCong = i + n;
      let contentHTML2 = /*html*/ `
                <td class="px-2">${soDuocCong}</td>
            `;
      content2 += contentHTML2;
    }
    n += 10;

    let contentHTML1 = /*html*/ `
            <tr>
                ${content2}
            </tr>
        `;
    content1 += contentHTML1;
  }

  document.getElementById("thongbao-bai1").innerHTML = /*html*/ `
            <table>
                ${content1}
            </table>
    `;
};

// Bài 2: In ra các số nguyên tố trong mảng
// Bước 1: Nhập số vào mảng
let arrayEx2 = [];
let nhapSoNguyenVaoMang = () => {
  let soNguyen = document.getElementById("songuyen-ex2").value;

  if (soNguyen == "") {
  } else {
    arrayEx2.push(soNguyen*1);
  }

  if(arrayEx2.length == 0){
    document.getElementById(
      "thongbao-manginput"
    ).innerHTML = `Vui lòng nhập số vào mảng!`;
  } else{
    document.getElementById(
      "thongbao-manginput"
    ).innerHTML = `Mảng hiện tại: ${arrayEx2}`;
  }
};

// Bước 2: In các số nguyên tố có trong mảng
let inSoNguyenTo = () => {
  let arrayEx2NT = [];

  arrayEx2.forEach(item => {
    if(item == 2 || item == 3 || item == 5 || item == 7){
      arrayEx2NT.push(item);
    } else if(item % 1 == 0 && item != 1 && item % 2 != 0 && item % 3 != 0 && item % 5 != 0 && item % 7 != 0){
      arrayEx2NT.push(item);
    }
  });

  if(arrayEx2NT.length == 0){
    document.getElementById(
      "thongbao-manginput"
    ).innerHTML = `Vui lòng nhập số vào mảng!`;
  } else{
    document.getElementById(
      "thongbao-songuyento"
    ).innerHTML = `Các số nguyên tố có trong mảng: ${arrayEx2NT}`;
  }
}

// Bài 3: Tính S theo tham số n
let tinhS = () => {
  let soN = document.getElementById("son-ex3").value *1;
  let soNCheck = document.getElementById("son-ex3").value;
  let tongTrongS = 0;
  let tinhS = 0;

  if(soNCheck == "" || soN < 0){
  } else if(soN == 0){
    tinhS = 0;
  } else if(soN == 1 || soN == 2){
    tinhS = soN + 2*soN;
  } else{
    for (let i = 2; i <= soN; i++) {
      tongTrongS += i;
    }
    tinhS = tongTrongS + 2*soN;
  }

  if(soNCheck == "" || soN < 0){
    document.getElementById("thongbao-ex3").innerHTML = `Vui lòng nhập số N hợp lệ!`
  } else{
    document.getElementById("thongbao-ex3").innerHTML = `S = ${tinhS}`
  }
}

// Bài 4: Tìm các ước số của N
let timUocSoN = () => {
  let arrayEx4 = [];
  let soN = document.getElementById("son-ex4").value *1;
  let soNCheck = document.getElementById("son-ex4").value;
  
  for (let i = 1; i <= soN; i++) {
    if(soN % i == 0){
      arrayEx4.push(i);
    }
  }

  if(soNCheck == "" || soN < 0){
    document.getElementById("thongbao-ex4").innerHTML = `Vui lòng nhập số N hợp lệ!`
  } else if(soN == 0){
    document.getElementById("thongbao-ex4").innerHTML = `Ước số của ${soN}: 0`
  } else{
    document.getElementById("thongbao-ex4").innerHTML = `Ước số của ${soN}: ${arrayEx4}`
  }
}

// Bài 5: Tìm số đảo ngược của 1 số nguyên dương n
let timSoDaoNguoc = () => {
  let soN = document.getElementById("son-ex5").value;
  let soNArr = [...(soN.toString())];

  let soNString = (soNArr.reverse());
  let soNOff = "";

  soNArr.forEach(item => {
    soNOff+=item;
  });
  console.log(soNOff);
  
  if(soN == "" || soN < 0){
    document.getElementById("thongbao-ex5").innerHTML = `Vui lòng nhập số N hợp lệ!`
  } else{
    document.getElementById("thongbao-ex5").innerHTML = `${soN} : ${soNOff}`
  }
}

// Bài 6: Tìm X nguyên dương lớn nhất
let timXLonNhat = () => {
  let tong = 0;
  let x = 0;

  for (let i = 1; i < 100; i++) {
    tong+=i;
    if(tong > 100){
      x = i-1;
      break;
    }
  }

  document.getElementById("thongbao-ex6").innerHTML = `Số X nguyên dương lớn nhất: ${x}`
}

// Bài 7: In ra bảng cửu chương
let inBangCuuChuong = () => {
  let soN = document.getElementById("son-ex7").value*1;
  let content = "";

  for (let i = 0; i <= 10; i++) {
    let tich = soN * i;
    let contentHTML = /*html*/ `
        <p>${soN} x ${i} = ${tich}</p>
     `
    content += contentHTML;
  }
  if(soN <= 0){
    document.getElementById("thongbao-ex7").innerHTML = `Vui lòng nhập số N hợp lệ!`;
  } else{
    document.getElementById("thongbao-ex7").innerHTML = content;
  }
}

// Bài 8: Chia bài cho 4 người chơi
let getRandom = (item) => {
  return item[Math.floor((Math.random()*item.length))];
}

let chiaBai = () => {
  let players = [ [], [], [], [] ];
  let cards = ["4K", "KH", "5C", "KA", "QH", "KD", "2H", "10S",
"AS", "7H", "9K", "10D"];

  players.forEach(item => {
    for (let index = 0; index < 3; index++) {
      let randomCard = getRandom(cards);
      let indexCard = cards.indexOf(randomCard);

      cards.splice(indexCard,1);
      item.push(randomCard);
    }
  });

  document.getElementById("thongbao-ex8").innerHTML = /*html*/ `
    <p>Player1 = ${players[0]}</p>
    <p>Player2 = ${players[1]}</p>
    <p>Player3 = ${players[2]}</p>
    <p>Player4 = ${players[3]}</p>
   `
}

// Bài 9: Tìm số gà, số chó
let timChoGa = () => {
  let tongChoGa = document.getElementById("tongchoga").value *1;
  let tongSoChan = document.getElementById("tongsochan").value *1;

  let soGa = null;
  let soCho = null;

  soCho = Math.floor((tongSoChan / 2) - tongChoGa);
  soGa = tongChoGa - soCho;

  if(tongChoGa < 1 || tongSoChan < 2 || soGa < 0 || soCho < 0){
    document.getElementById("thongbao-ex9").innerHTML = "Vui lòng nhập số liệu hợp lệ!"
  } else{
    document.getElementById("thongbao-ex9").innerHTML = /*html*/ `
      <p>Số chó: ${soCho} con</p>
      <p>Số gà: ${soGa} con</p>
    `
  }
}

// Bài 10: Tìm góc lệch tính từ kim giờ đến kim phút
let timGocLech = () => {
  let soGio = document.getElementById("sogio").value *1;
  let soPhut = document.getElementById("sophut").value *1;
  let soGioRad = 0;
  let soPhutRad = 0;

  if(soGio > 12){
    soGioRad = (soGio-12) * 30;
    soPhutRad = soPhut * 6;
  } else{
    soGioRad = soGio * 30;
    soPhutRad = soPhut * 6;
  }

  if(soGio < 0 || soGio >= 24 || soPhut < 0 || soPhut >= 60){
    document.getElementById("thongbao-ex10").innerHTML = `Vui lòng nhập số liệu hợp lệ!`
  } else{
    document.getElementById("thongbao-ex10").innerHTML = `Góc lệch tính từ kim giờ đến kim phút: ${Math.abs(soGioRad - soPhutRad)} độ!`
  }
  
}
